bestEverSystemAPI = container "API" {
    description "API for best ever system"
    technology "Kotlin"
    tags "Backend"

    !include ../components/audit-lake-client.dsl

    -> centralDB "Persist data"
}
