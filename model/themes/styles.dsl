theme default

styles {
    element "deprecated" {
        background #FF0000
        color #ffffff
        shape RoundedBox
    }

    element "not-implemented" {
        background #ffb09c
        color #ffffff
        shape RoundedBox
    }

    relationship "not-implemented" {
        color #ffb09c
    }
}