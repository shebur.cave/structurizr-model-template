registerWebApp = container "Register WebApp" {
    description "User Web App for Register integration"
    technology "React"
    tags "Frontend"

    -> registerAPI ""

    employee -> this "Check data flows"
}
