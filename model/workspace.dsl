workspace {

    model {

        properties {
            "structurizr.groupSeparator" "/"
        }
        
        # persons
        !include elements/actors.dsl

        # external systems
        !include elements/systems/extRegister.dsl

        # in-house systems
        !include elements/systems/auditLake.dsl
        !include elements/systems/bestEverSystem.dsl
    }

    views {

        systemLandscape landscape "System Landscape" {
            include *
            autolayout lr
        }

        systemContext bestEverSystem {
            include *
            autolayout lr
        }

        container bestEverSystem {
            include *
            autolayout lr
        }

        container auditLake {
            include *
            autolayout lr
        }

        component bestEverSystemAPI {
            include *
            autolayout lr
        }

        !include themes/styles.dsl
    }

}