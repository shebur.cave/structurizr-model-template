auditAPIGateway = container "Audit API Gateway" {
    description "Gateway to audit log database"
    technology "Java 17"
    tags "not-implemented"

    perspectives {
        "Audit" "Processs audit requests and save ot fetch audit entries"
    }

    -> auditDB "Save/Fetch data"
}
