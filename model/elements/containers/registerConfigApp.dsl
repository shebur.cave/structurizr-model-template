registerConfigApp = container "Register Config App" {
    description "Admin Web App for configuring Register integration"
    technology "React"
    tags "Frontend"

    -> registerAPI ""

    employee -> this "Manage integration configuration"
}
