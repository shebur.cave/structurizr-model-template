component "audit-lake-client" {
        description "Audit Lake Client library"
        technology "Kotlin"
        tags "Backend" "Library"

        -> auditAPIGateway "Post audit events"
}
