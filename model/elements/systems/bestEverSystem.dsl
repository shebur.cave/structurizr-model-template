bestEverSystem = softwareSystem "Best Ever System" {

        # Datastores
        !include ../datastores/centralDB.dsl

        # 
        !include ../containers/bestEverSystemAPI.dsl
        !include ../containers/bestEverSystemWebApp.dsl

        group "Register - integration" {
                !include ../containers/registerAPI.dsl
                !include ../containers/registerConfigApp.dsl
                !include ../containers/registerWebApp.dsl
        }

}