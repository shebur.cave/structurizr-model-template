bestEverSystemWebApp = container "WebApp" {
    description "Web App for best ever system"
    technology "React"
    tags "Frontend"
   
    -> bestEverSystemAPI ""

    employee -> this "Check data flows"
    customer -> this "Create data flows"
}
