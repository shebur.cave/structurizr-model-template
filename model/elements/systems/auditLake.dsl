auditLake = softwareSystem "Audit LakeHouse" {
    tags "not-implemented"

    !include ../datastores/auditDB.dsl
    !include ../containers/auditAPIGateway.dsl

    customerSupport -> this "Browse Audit Logs"
}
