registerAPI = container "Register API" {
    description "Backend part of integration with some system 'Register'"
    technology "Kotlin"
    tags "Backend"

    !include ../components/audit-lake-client.dsl

    -> extRegister "Exchange data"
}
